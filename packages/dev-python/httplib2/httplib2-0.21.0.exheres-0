# Copyright 2011-2019 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools test=pytest ]

SUMMARY="A comprehensive HTTP client library in Python"
DESCRIPTION="
A comprehensive HTTP client library, httplib2.py supports many features left out of other HTTP
libraries.

HTTP and HTTPS
    HTTPS support is only available if the socket module was compiled with SSL support.
Keep-Alive
    Supports HTTP 1.1 Keep-Alive, keeping the socket open and performing multiple requests over the
    same connection if possible.
Authentication
    The following three types of HTTP Authentication are supported. These can be used over both HTTP
    and HTTPS.

        * Digest
        * Basic
        * WSSE

Caching
    The module can optionally operate with a private cache that understands the Cache-Control:
    header and uses both the ETag and Last-Modified cache validators.
All Methods
    The module can handle any HTTP request method, not just GET and POST.
Redirects
    Automatically follows 3XX redirects on GETs.
Compression
    Handles both 'deflate' and 'gzip' types of compression.
Lost update support
    Automatically adds back ETags into PUT requests to resources we have already cached. This
    implements Section 3.2 of Detecting the Lost Update Problem Using Unreserved Checkout.
Unit Tested
    A large and growing set of unit tests.
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

# pytest-randomly unpackaged
DEPENDENCIES="
    build+run:
        python_abis:2.7? ( dev-python/pyparsing[>=2.4.2&<3][python_abis:2.7] )
        python_abis:3.8? ( dev-python/pyparsing[>=2.4.2&<4][python_abis:3.8] )
        python_abis:3.9? ( dev-python/pyparsing[>=2.4.2&<4][python_abis:3.9] )
        python_abis:3.10? ( dev-python/pyparsing[>=2.4.2&<4][python_abis:3.10] )
        python_abis:3.11? ( dev-python/pyparsing[>=2.4.2&<4][python_abis:3.11] )
    test:
        dev-python/cryptography[>=35.0.0][python_abis:*(-)?]
        dev-python/flake8[>=3.9.2][python_abis:*(-)?]
        dev-python/future[>=0.16.0][python_abis:*(-)?]
        dev-python/pytest-cov[>=2.12.1][python_abis:*(-)?]
        dev-python/pytest-forked[>=1.3.0][python_abis:*(-)?]
        dev-python/pytest-timeout[>=1.4.2][python_abis:*(-)?]
        dev-python/pytest-xdist[>=1.34.0][python_abis:*(-)?]
        dev-python/six[>=1.10.0][python_abis:*(-)?]
        python_abis:2.7? ( dev-python/mock[>=2.0.0][python_abis:2.7] )
"

# Tests run against a server on the internet.
# Running the tests locally means manually setting up a carefully-configured
# server and patching the tests to use localhost.
RESTRICT="test"

setup-py_test_one_multibuild() {
    local version=$(ever major $(python_get_abi))
    PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo ${PYTHON} -B python${version}/httplib2test.py
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # Fix permissions
    edo chmod 644 "${IMAGE}$(python_get_sitedir)"/httplib2/cacerts.txt
    edo chmod o+r "${IMAGE}$(python_get_sitedir)"/*egg*/*
}

