# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pylint-1.7.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

# Tarball from pypi doesn't contain the tests
#require pypi
require github [ user=PyCQA tag=v${PV} ]
require py-pep517 [ backend=setuptools test=pytest entrypoints=[ epylint pylint pylint-config pyreverse symilar ] ]

SUMMARY="a python code static checker"
DESCRIPTION="
Pylint is a Python source code analyzer which looks for programming errors, helps enforcing a coding
standard and sniffs for some code smells (as defined in Martin Fowler's Refactoring book).

It's highly configurable and handle pragmas to control it from within your code.
Additionally, it is possible to write plugins to add your own checks.
"
HOMEPAGE="https://www.pylint.org/"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"

MYOPTIONS=""

# tomli>=1.1.0;python_version<"3.11"
DEPENDENCIES="
    build+run:
        dev-python/astroid[>=2.15.4&<2.17.0][python_abis:*(-)?]
        dev-python/dill[>=0.3.6][python_abis:*(-)?]
        dev-python/isort[>=4.2.5&<6][python_abis:*(-)?]
        dev-python/mccabe[>=0.6.0&<0.8][python_abis:*(-)?]
        dev-python/platformdirs[>=2.2.0][python_abis:*(-)?]
        dev-python/tomlkit[>=0.10.1][python_abis:*(-)?]
        python_abis:3.8? (
            dev-python/tomli[>=1.1.0][python_abis:3.8]
            dev-python/typing-extensions[>=3.10][python_abis:3.8]
        )
        python_abis:3.9? (
            dev-python/tomli[>=1.1.0][python_abis:3.9]
            dev-python/typing-extensions[>=3.10][python_abis:3.9]
        )
        python_abis:3.10? ( dev-python/tomli[>=1.1.0][python_abis:3.10] )
    test:
        dev-python/GitPython[>=3][python_abis:*(-)?]
        dev-python/py[>=1.11.0][python_abis:*(-)?]
        dev-python/pytest[>=7.2][python_abis:*(-)?]
        dev-python/pytest-benchmark[>=4.0][python_abis:*(-)?]
        dev-python/pytest-timeout[>=2.1][python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-python/towncrier[>=22.12][python_abis:*(-)?]
"

# those contain errors for testing pylint. Obviously, those are breaking byte-compilation
PYTHON_BYTECOMPILE_EXCLUDES=( test/functional test/input test/regrtest_data )

PYTEST_PARAMS=(
    --ignore=pylint/ # do not pick duplicates for files in PYTHONPATH
)
PYTEST_SKIP=(
    # TODO: Find out why they fail
    test_functional[no_name_in_module]
    test_functional[dataclass_with_field]
)

test_one_multibuild() {

    esandbox allow_net "unix:${TEMP%/}/pymp-*/listener-*"
    esandbox allow_net --connect "unix:${TEMP%/}/pymp-*/listener-*"

    py-pep517_test_one_multibuild

    esandbox disallow_net "unix:${TEMP%/}/pymp-*/listener-*"
    esandbox disallow_net --connect "unix:${TEMP%/}/pymp-*/listener-*"
}

