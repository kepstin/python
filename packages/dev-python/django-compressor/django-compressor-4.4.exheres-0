# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/_}
MY_PNV=${MY_PN}-${PV}

require pypi setup-py [ import=setuptools blacklist=2 work=${MY_PNV} ]

SUMMARY="Compresses linked and inline javascript or CSS into a single cached file"
DESCRIPTION="
Django Compressor processes, combines and minifies linked and inline
Javascript or CSS in a Django template into cacheable static files.
It supports compilers such as coffeescript, LESS and SASS and is extensible
by custom processing steps."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/django-appconf[>=1.0.3][python_abis:*(-)?]
        dev-python/rcssmin[>=1.1.1][python_abis:*(-)?] [[
            note = [ == 1.1.1 according to setup.py but that's annoying ]
        ]]
        dev-python/rjsmin[>=1.2.1][python_abis:*(-)?] [[
            note = [ == 1.2.1 according to setup.py but that's annoying ]
        ]]
    test:
        app-arch/brotli[python][python_abis:*(-)?]
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/beautifulsoup4[python_abis:*(-)?]
        dev-python/csscompressor[python_abis:*(-)?]
        dev-python/django-sekizai[python_abis:*(-)?]
        dev-python/flake8[python_abis:*(-)?]
        dev-python/html5lib[python_abis:*(-)?]
        dev-python/lxml[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Add-LazyScriptNamePrefixedUrl.lstrip-to-fix-tests-11.patch
)

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # Avoid the calmjs (unwritten) test dependency for now
    edo sed -e 's/test_calmjs_filter/_&/' -i compressor/tests/test_filters.py
}

test_one_multibuild() {
    PYTHONPATH=. \
        edo ${PYTHON} /usr/$(exhost --build)/bin/django-admin.py test -v2 \
             --settings=compressor.test_settings compressor
}

