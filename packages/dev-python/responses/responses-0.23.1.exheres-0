# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ blacklist=2 import=setuptools test=pytest ]

SUMMARY="A utility library for mocking out the \`requests\` Python library"
HOMEPAGE+=" https://github.com/getsentry/responses"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/PyYAML[python_abis:*(-)?]
        dev-python/requests[>=2.22&<3.0][python_abis:*(-)?]
        dev-python/types-PyYAML[python_abis:*(-)?]
        dev-python/urllib3[>=1.25.10][python_abis:*(-)?]
    test:
        dev-python/coverage[>=6.0.0][python_abis:*(-)?]
        dev-python/flake8[python_abis:*(-)?]
        dev-python/mypy[python_abis:*(-)?]
        dev-python/pytest[>=7.0.0][python_abis:*(-)?]
        dev-python/pytest-asyncio[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/pytest-httpserver[python_abis:*(-)?]
        dev-python/tomli-w[python_abis:*(-)?]
        dev-python/types-requests[python_abis:*(-)?]
        python_abis:3.8? ( dev-python/toml[python_abis:3.8] )
        python_abis:3.9? ( dev-python/toml[python_abis:3.9] )
        python_abis:3.10? ( dev-python/toml[python_abis:3.10] )
"

PYTEST_PARAMS=(
    # It doesn't depend on pytest-localserver anymore, but if it's installed
    # it makes the tests fail with some AttributeError...
    -p no:localserver
)

