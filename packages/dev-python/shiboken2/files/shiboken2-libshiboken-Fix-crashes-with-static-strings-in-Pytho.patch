Upstream: yes, cherry-picked from dev, but there's no public branch for 5.15.x

From a09a1db8391243e6bb290ee66bb6e3afbb114c61 Mon Sep 17 00:00:00 2001
From: Friedemann Kleint <Friedemann.Kleint@qt.io>
Date: Fri, 24 Jun 2022 09:22:01 +0200
Subject: [PATCH] libshiboken: Fix crashes with static strings in Python 3.11

In Python 3.11, some strings come with a refcount above decimal
1000000000, apparently indicating that they are interned. Replace the
mechanism by PyUnicode_InternFromString().

Task-number: PYSIDE-1960
Pick-to: 6.3 6.2 5.15
Change-Id: I6436afee351f89da5814b5d6bc76970b1b508168
Reviewed-by: Qt CI Bot <qt_ci_bot@qt-project.org>
Reviewed-by: Christian Tismer <tismer@stackless.com>
---
 sources/shiboken2/libshiboken/sbkstring.cpp | 19 +++++++++++++++++--
 1 file changed, 17 insertions(+), 2 deletions(-)

diff --git a/sources/shiboken2/libshiboken/sbkstring.cpp b/sources/shiboken2/libshiboken/sbkstring.cpp
index 7fd20173c..36fe50369 100644
--- a/sources/shiboken2/libshiboken/sbkstring.cpp
+++ b/sources/shiboken2/libshiboken/sbkstring.cpp
@@ -5,8 +5,14 @@
 #include "sbkstaticstrings_p.h"
 #include "autodecref.h"
 
-#include <vector>
-#include <unordered_set>
+#if PY_VERSION_HEX >= 0x030B0000 || defined(Py_LIMITED_API)
+#  define USE_INTERN_STRINGS
+#endif
+
+#ifndef USE_INTERN_STRINGS
+#  include <vector>
+#  include <unordered_set>
+#endif
 
 namespace Shiboken::String
 {
@@ -179,6 +185,13 @@ Py_ssize_t len(PyObject *str)
 //     PyObject *attr = PyObject_GetAttr(obj, name());
 //
 
+#ifdef USE_INTERN_STRINGS
+PyObject *createStaticString(const char *str)
+{
+     return PyUnicode_InternFromString(str);
+}
+#else
+
 using StaticStrings = std::unordered_set<PyObject *>;
 
 static void finalizeStaticStrings();    // forward
@@ -225,6 +238,8 @@ PyObject *createStaticString(const char *str)
     return result;
 }
 
+#endif // !USE_INTERN_STRINGS
+
 ///////////////////////////////////////////////////////////////////////
 //
 // PYSIDE-1019: Helper function for snake_case vs. camelCase names
-- 
2.40.0

