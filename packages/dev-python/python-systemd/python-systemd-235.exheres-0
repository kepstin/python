# Copyright 2016 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=systemd tag=v${PV} ]
require setup-py [ import=setuptools test=pytest ]

SUMMARY="Python module for native access to the systemd facilities"
DESCRIPTION="
Functionality is seperated into a number of modules:
    systemd.journal supports sending of structured messages to the journal and reading journal files,
    systemd.daemon wraps parts of libsystemd useful for writing daemons and socket activation,
    systemd.id128 provides functions for querying machine and boot identifiers and a lists of message identifiers provided by systemd,
    systemd.login wraps parts of libsystemd used to query logged in users and available seats and machines
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        sys-apps/systemd
"

PYTEST_PARAMS=( systemd/test )

test_one_multibuild() {
    # all of this is to avoid "error: AF_UNIX path too long"
    local oldtmp=${TMPDIR}
    TMPDIR=$(mktemp --dry-run /tmp/tmp.XXXX)

    edo esandbox allow_net --connect unix:/run/systemd/journal/stdout
    edo esandbox allow "${TMPDIR}"
    edo esandbox allow_net unix:"${TMPDIR}"/pytest-of-paludisbuild/pytest-0/test_notify_with_socket0/socket
    edo mkdir "${TMPDIR}"

    nonfatal setup-py_test_one_multibuild || local failed=yes

    edo rm -rf "${TMPDIR}"
    esandbox disallow_net unix:"${TMPDIR}"/pytest-of-paludisbuild/pytest-0/test_notify_with_socket0/socket
    esandbox disallow "${TMPDIR}"
    esandbox disallow_net --connect unix:/run/systemd/journal/stdout

    TMPDIR=${oldtmp}

    [[ -n ${failed} ]] && die "tests failed"
}

