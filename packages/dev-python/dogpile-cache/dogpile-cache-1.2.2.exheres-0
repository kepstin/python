# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/.}
MY_PNV=${MY_PN}-${PV}

require pypi setup-py [ blacklist=2 import=setuptools test=pytest work=${MY_PNV} ]

SUMMARY="A caching front-end based on the Dogpile lock"
DESCRIPTION="
Dogpile consists of two subsystems, one building on top of the other.

dogpile provides the concept of a “dogpile lock”, a control structure which
allows a single thread of execution to be selected as the “creator” of some
resource, while allowing other threads of execution to refer to the
previous version of this resource as the creation proceeds; if there is no
previous version, then those threads block until the object is available.

dogpile.cache is a caching API which provides a generic interface to caching
backends of any variety, and additionally provides API hooks which integrate
these cache backends with the locking mechanism of dogpile.

Overall, dogpile.cache is intended as a replacement to the Beaker caching
system, the internals of which are written by the same author. All the ideas
of Beaker which “work” are re- implemented in dogpile.cache in a more
efficient and succinct manner, and all the cruft (Beaker’s internals were
first written in 2005) relegated to the trash heap."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/decorator[>=4.0.0][python_abis:*(-)?]
        dev-python/stevedore[>=3.0.0][python_abis:*(-)?]
        python_abis:3.8? ( dev-python/typing-extensions[>=4.0.1][python_abis:3.8] )
        python_abis:3.9? ( dev-python/typing-extensions[>=4.0.1][python_abis:3.9] )
        python_abis:3.10? ( dev-python/typing-extensions[>=4.0.1][python_abis:3.10] )
    test:
        dev-python/Mako[python_abis:*(-)?]
        dev-python/pytest[>=5.4][python_abis:*(-)?]
"

