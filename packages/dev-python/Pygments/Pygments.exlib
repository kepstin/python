# Copyright 2008, 2009, 2010 Ali Polatel
# Copyright 2015-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2020 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require bash-completion pypi
if ever at_least 2.15.0 ; then
    require py-pep517 [ backend=setuptools test=pytest entrypoints=[ pygmentize ] ]
else
    require setup-py [ import=setuptools blacklist=none test=nose has_bin=true ]
fi

SUMMARY="Syntax highlighting package written in Python"
HOMEPAGE=" https://pygments.org"
LICENCES="BSD-2"
DESCRIPTION="
Pygments is a generic syntax highlighter for general use in all kinds of
software such as forum systems, wikis or other applications that need to
prettify source code. Highlights are:
* A wide range of common languages and markup formats is supported.
* Special attention is paid to details that increase highlighting quality.
* Support for new languages and formats are added easily; most languages use a
  simple regex-based lexing mechanism.
* A number of output formats is available, among them HTML, RTF, LaTeX and ANSI
  sequences.
* It is usable as a command-line tool and as a library.
* ... and it highlights even Brainf*ck!
"
REMOTE_IDS="pypi:Pygments"

SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

if ever at_least 2.15.0 ; then
    DEPENDENCIES+="
        test:
            dev-python/pytest[>=7.0][python_abis:*(-)?]
            dev-python/wcag-contrast-ratio[python_abis:*(-)?]
    "
fi

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( CHANGES )

test_one_multibuild() {
    if ever at_least 2.15.0 ; then
        py-pep517_test_one_multibuild
    else
        # Skip broken test_cmdline on Python 2.x
        # https://bitbucket.org/birkenfeld/pygments-main/issues/1492
        if [[ $(python_get_abi) == 2.* ]]; then
            edo rm tests/test_cmdline.py
        fi
        emake -j1 test
    fi
}

install_one_multibuild() {
    if ever at_least 2.15.0 ; then
        py-pep517_install_one_multibuild
    else
        setup-py_install_one_multibuild
    fi

    doman doc/pygmentize.1

    dobashcompletion external/pygments.bashcomp pygments
}

