# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=setuptools test=pytest entrypoints=[ ${PN} ] ]

SUMMARY="Versioning It with your Version In Git"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/packaging[>=17.1][python_abis:*(-)?]
        python_abis:3.8? (
            dev-python/importlib_metadata[>=3.6][python_abis:3.8]
            dev-python/tomli[>=1.2&<3][python_abis:3.8]
        )
        python_abis:3.9? (
            dev-python/importlib_metadata[>=3.6][python_abis:3.9]
            dev-python/tomli[>=1.2&<3][python_abis:3.9]
        )
        python_abis:3.10? (
            dev-python/tomli[>=1.2&<3][python_abis:3.10]
        )
    test:
        dev-python/build[python_abis:*(-)?]
        dev-python/pydantic[>=1.8][python_abis:*(-)?]
        dev-python/pytest[>=7][python_abis:*(-)?]
"

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    edo sed -e "/--cov/d" -i tox.ini
}

PYTEST_SKIP=(
    # Wants to download packages via pip
    test_editable_mode[cmd0]
    # Wants to write to the systemd python site-packages dirs
    test_editable_mode[cmd1]
)

