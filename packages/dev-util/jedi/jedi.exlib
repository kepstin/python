# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require pypi
require setup-py [ import=setuptools blacklist=2 python_opts="[readline]" test=pytest ]

SUMMARY="Awesome autocompletion library for python"
HOMEPAGE+=" https://jedi.readthedocs.org/"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

# Also wants unpackaged numpydoc for tests, but works without it too.
DEPENDENCIES="
    build+run:
        dev-python/parso[>=0.8.0][python_abis:*(-)?]
    test:
        dev-python/colorama[>=0.4.1][python_abis:*(-)?]
        dev-python/docopt[python_abis:*(-)?]
        dev-python/pytest[>=3.9.0&<7][python_abis:*(-)?] 
"

PYTEST_PARAMS=(
    # FIXME(marvs): Fails a single test for me because `testtools` and `testscenarios` are installed
    # and picked up as completions, see https://github.com/davidhalter/jedi/issues/1658
    -k "not test_completion"
)

# Sphinx pulls in Pygments, which depends on pytest[>=7], which doesn't work
# with jedi yet (that <7 requirement still lives in upstream git).
# I'm willing to wager that Sphinx and Pygments are more commonly used, so
# restrict the tests here until jedi work with pytest[>=7].
RESTRICT="test"

